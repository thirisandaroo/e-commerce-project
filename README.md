# E-commerce-project


## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/thirisandaroo/e-commerce-project.git
git branch -M master
git push -uf origin master
```

## PostMan Collection

[ Download the postman collection link](https://drive.google.com/file/d/1gAEOqQB-k8kRPNP9wUc2ILAaZkbarlr1/view?usp=sharing)

### Project Summary

This is the e-commerce sample project api.You can create,read,update and delete the product category, product and cart items.

### Technology Stack

Laravel and MySQL 

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
